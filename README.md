# Qulture Rocks Challenge

## Prerequisites

[NodeJS v12.18+](https://nodejs.org/en/) (Using [NVM](https://github.com/nvm-sh/nvm) is recommended)

[Docker + Docker Compose](https://www.docker.com/)

## How does the segment creation work?

Given the REST JSON constraint the following payload should be used to create custom segments.

```json
{
  "name": "example_segment",
  "segments": [
    {
      "firstName": "John",
      "lastName": "Doe",
      "email": "john.doe@email.com",
      "birthDate": {
        "before": "2016-10-02",
        "after": "2015-10-02"
      },
      "admissionDate": {
        "before": "2016-10-02",
        "after": "2015-10-02"
      },
      "isActive": true,
      "sex": "male",
      "lastSignInAt": {
        "amount": 3,
        "period": "days"
      },
      "tags": [
        "string"
      ]
    },
    {
      ...
    }
  ]
}
```

In the above example all properties are provided to show that it is possible to create such query.

The way it is designed is so that every property within the same segment will generate an AND clause and all segments will generate an OR clause, to make it easier to understand given the following spec:

```json
{
  "name": "simple_spec",
  "segments": [
    { "firstName": "example", "lastName": "spec" },
    { "firstName": "simple", "lastName": "spec" }
  ]
}
```

Will generate a segment called "simple_spec" with the following query:

```sql
CREATE OR REPLACE VIEW "simple_spec" AS
SELECT * FROM users WHERE
(first_name = 'example' AND last_name = 'spec')
OR (first_name = 'simple' AND last_name = 'spec');
```

In a nutshell a "segment" is simply a dynamically created view that is associated with an entry in the segments table, this table exists solely to keep track of existing segments.

## Running the application

```bash
# Run the database (postgres)
docker-compose up

# Set node version or skip this step to use the one already installed (not recommended)
nvm use

# If you do not have this version installed run
nvm install

# Install dependencies
npm install

# Apply the migrations
npm run migrate:run

# Run the application
npm start
```

Go to [Documentation Page](http://localhost:3000/docs) to view all available endpoints and live test the application in Swagger UI.

## Running tests

```bash
# Run tests
npm run test:e2e
```

## Running in production mode

```bash
# Build the application
npm run build

# Run production build
npm run start:prod
```

## Future improvements and issues

- Using JSON for the spec creation has limitations and can get pretty complicated to parse and maintain as the spec grows.
- The query builder code is not 100% bulletproof, if someone changes it and forgets to escape values we could end up opening ourselves to SQLInjection. A first solution to this problem could be using typeorm's query builder.
- The query builder adds a pesky "1 = 1" to the beggining of every AND clause because it is used as the initial value for the reduce that is applied in the segment, up until now all other solutions would require a bunch more if statements which would make the code worse.
- In general creating a DSL instead of using JSON probably would make the code better and easier to maintain, but then we would almost be recreating GraphQL.
