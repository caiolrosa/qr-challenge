import {
  registerDecorator,
  ValidationOptions,
  isNotEmpty,
} from 'class-validator';
import * as moment from 'moment';

export function IsShortDateString(validationOptions?: ValidationOptions) {
  return function(object: any, propertyName: string): void {
    const dateFormat = 'YYYY-MM-DD';
    registerDecorator({
      name: 'isShortDateString',
      target: object.constructor,
      propertyName: propertyName,
      options: {
        message: `${propertyName} must be in the format ${dateFormat}`,
        ...validationOptions,
      },
      validator: {
        validate(value: any) {
          return isNotEmpty(value) && moment(value, dateFormat, true).isValid();
        },
      },
    });
  };
}
