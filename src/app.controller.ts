import { Controller, Get } from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';

@Controller()
export class AppController {
  @ApiResponse({ status: 200, description: 'Service is running...' })
  @Get('healthcheck')
  healthCheck(): string {
    return 'Service running...';
  }
}
