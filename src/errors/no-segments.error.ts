import { NotFoundException } from '@nestjs/common';

export class NoSegmentsError extends NotFoundException {}
