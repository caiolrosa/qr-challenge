import { NestFactory, Reflector } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as helmet from 'helmet';
import { ValidationPipe, ClassSerializerInterceptor } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { QueryFailedExceptionFilter } from './filters/query-failed.exception.filter';
import { TypeErrorExceptionFilter } from './filters/type-error.exception.filter';

function getSwaggerOptions() {
  return new DocumentBuilder()
    .setTitle('Qulture Rocks Challenge')
    .setDescription('Qulture Rocks API documentation')
    .setVersion('1.0')
    .build();
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin:
      process.env.NODE_ENV === 'production' ? 'https://some.prod.url' : '*',
  });
  app.use(helmet());
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  app.useGlobalInterceptors(new ClassSerializerInterceptor(app.get(Reflector)));
  app.useGlobalFilters(
    new QueryFailedExceptionFilter(),
    new TypeErrorExceptionFilter(),
  );

  const swaggerDocument = SwaggerModule.createDocument(
    app,
    getSwaggerOptions(),
  );

  SwaggerModule.setup('docs', app, swaggerDocument);

  const configService: ConfigService = app.get(ConfigService);
  await app.listen(configService.get('PORT', 3000));
}
bootstrap();
