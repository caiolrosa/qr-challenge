import { MigrationInterface, QueryRunner } from 'typeorm';

export class createUserTable1597711804978 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const query = `
      BEGIN TRANSACTION;

      DROP TYPE IF EXISTS sex;
      CREATE TYPE sex AS ENUM ('male', 'female');

      CREATE TABLE IF NOT EXISTS users (
        id bigserial PRIMARY KEY,
        first_name text NOT NULL,
        last_name text NOT NULL,
        email text NOT NULL UNIQUE,
        birth_date date NOT NULL,
        admission_date date NOT NULL,
        is_active boolean DEFAULT TRUE,
        sex sex NOT NULL,
        last_sign_in_at timestamp with time zone DEFAULT NOW()
      );

      COMMIT;
    `;
    await queryRunner.query(query);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const query = `
      BEGIN TRANSACTION;

      DROP TABLE IF EXISTS users CASCADE;
      DROP TYPE IF EXISTS sex;

      COMMIT;
    `;
    await queryRunner.query(query);
  }
}
