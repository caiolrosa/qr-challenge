import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTagTable1597711812446 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const query = `
      CREATE TABLE IF NOT EXISTS tags (
        id bigserial PRIMARY KEY,
        name text NOT NULL UNIQUE
      );
    `;
    await queryRunner.query(query);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const query = 'DROP TABLE IF EXISTS tags CASCADE';
    await queryRunner.query(query);
  }
}
