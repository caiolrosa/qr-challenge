import { MigrationInterface, QueryRunner } from 'typeorm';

export class createUserTagRelationship1597715248862
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const query = `
      CREATE TABLE IF NOT EXISTS user_tag (
        user_id bigint NOT NULL,
        tag_id bigint NOT NULL,
        FOREIGN KEY (user_id) REFERENCES users (id),
        FOREIGN KEY (tag_id) REFERENCES tags (id)
      );
    `;
    await queryRunner.query(query);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const query = 'DROP TABLE IF EXISTS user_tag CASCADE';
    await queryRunner.query(query);
  }
}
