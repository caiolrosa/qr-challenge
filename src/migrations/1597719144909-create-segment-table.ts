import { MigrationInterface, QueryRunner } from 'typeorm';

export class createSegmentTable1597719144909 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const query = `
      CREATE TABLE IF NOT EXISTS segments (
        id bigserial PRIMARY KEY,
        name text NOT NULL UNIQUE
      );
    `;
    await queryRunner.query(query);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const query = 'DROP TABLE IF EXISTS segments CASCADE';
    await queryRunner.query(query);
  }
}
