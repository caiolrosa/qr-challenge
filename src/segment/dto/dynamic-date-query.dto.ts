import { IsNumber, IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export enum DatePeriod {
  Seconds = 'seconds',
  Minutes = 'minutes',
  Hours = 'hours',
  Days = 'days',
}

export class DynamicDateQueryDTO {
  @IsNumber()
  @ApiProperty({ example: 3 })
  amount: number;

  @IsEnum(DatePeriod)
  @ApiProperty({ enum: DatePeriod, example: 'days' })
  period: DatePeriod;

  static isDynamicQueryDTO(obj: any): obj is DynamicDateQueryDTO {
    return (
      (obj as DynamicDateQueryDTO).amount !== undefined &&
      (obj as DynamicDateQueryDTO).period !== undefined
    );
  }
}
