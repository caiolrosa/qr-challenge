import { IsNotEmpty, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { UserSegmentDTO } from './user-segment.dto';
import { ApiProperty } from '@nestjs/swagger';

export class SegmentCreateDTO {
  @IsNotEmpty()
  @ApiProperty({ example: 'example_segment' })
  name: string;

  @ValidateNested({ each: true })
  @Type(() => UserSegmentDTO)
  @ApiProperty({ type: [UserSegmentDTO] })
  segments: UserSegmentDTO[];
}
