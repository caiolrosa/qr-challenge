import { IsOptional } from 'class-validator';
import { IsShortDateString } from '../../annotations/data-validation.annotation';
import { ApiProperty } from '@nestjs/swagger';

export class StaticDateQueryDTO {
  @IsOptional()
  @IsShortDateString()
  @ApiProperty({ example: '2016-10-02' })
  before?: string;

  @IsOptional()
  @IsShortDateString()
  @ApiProperty({ example: '2015-10-02' })
  after?: string;

  static isStaticQueryDTO(obj: any): obj is StaticDateQueryDTO {
    return (
      (obj as StaticDateQueryDTO).before !== undefined ||
      (obj as StaticDateQueryDTO).after !== undefined
    );
  }
}
