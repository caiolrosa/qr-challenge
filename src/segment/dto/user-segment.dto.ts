import {
  IsOptional,
  IsEmail,
  IsEnum,
  IsArray,
  IsNotEmpty,
  IsBoolean,
  ValidateNested,
} from 'class-validator';
import { Sex } from '../../user/user.entity';
import { StaticDateQueryDTO } from './static-date-query.dto';
import { DynamicDateQueryDTO } from './dynamic-date-query.dto';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export class UserSegmentDTO {
  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({ example: 'John' })
  firstName?: string;

  @IsOptional()
  @IsNotEmpty()
  @ApiProperty({ example: 'Doe' })
  lastName?: string;

  @IsOptional()
  @IsEmail()
  @ApiProperty({ example: 'john.doe@email.com' })
  email?: string;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => StaticDateQueryDTO)
  @ApiProperty()
  birthDate?: StaticDateQueryDTO;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => StaticDateQueryDTO)
  @ApiProperty()
  admissionDate?: StaticDateQueryDTO;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({ example: true })
  isActive?: boolean;

  @IsOptional()
  @IsEnum(Sex)
  @ApiProperty({ enum: Sex, example: 'male' })
  sex?: Sex;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => DynamicDateQueryDTO)
  @ApiProperty()
  lastSignInAt?: DynamicDateQueryDTO;

  @IsOptional()
  @IsArray()
  @ApiProperty({ type: [String] })
  tags?: string[];
}
