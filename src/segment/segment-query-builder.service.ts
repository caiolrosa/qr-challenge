import { Injectable } from '@nestjs/common';
import { DynamicDateQueryDTO } from './dto/dynamic-date-query.dto';
import { StaticDateQueryDTO } from './dto/static-date-query.dto';
import { SegmentCreateDTO } from './dto/segment-create.dto';
import { UserSegmentDTO } from './dto/user-segment.dto';
import { NoSegmentsError } from '../errors/no-segments.error';
import { isNumber, isArray } from 'class-validator';
import { isBoolean } from 'util';

@Injectable()
export class SegmentQueryBuilderService {
  private readonly USER_ALIAS = 'u';
  private readonly TAG_ALIAS = 't';
  private readonly USER_TAG_ALIAS = 'ut';

  buildSegmentQuery(segmentCreateDTO: SegmentCreateDTO) {
    const conditional = this.buildConditionalQuery(segmentCreateDTO.segments);
    return `
      CREATE OR REPLACE VIEW "${segmentCreateDTO.name}"
      AS SELECT ${this.USER_ALIAS}.* FROM users AS ${this.USER_ALIAS}
      LEFT JOIN user_tag AS ${this.USER_TAG_ALIAS} ON ${this.USER_ALIAS}.id = ${this.USER_TAG_ALIAS}.user_id
      LEFT JOIN tags AS ${this.TAG_ALIAS} ON ${this.TAG_ALIAS}.id = ${this.USER_TAG_ALIAS}.tag_id ${conditional};
    `;
  }
  private buildConditionalQuery(segments: UserSegmentDTO[]): string {
    if (segments.length === 0) {
      throw new NoSegmentsError(
        'No segments were provided to buildConditionalQuery',
      );
    }

    const initialStatement = 'WHERE';
    return segments.reduce((acc, segment) => {
      if (acc === initialStatement) {
        return `${acc} (${this.buildANDQuery(segment)})`;
      }
      return `${acc} OR (${this.buildANDQuery(segment)})`;
    }, initialStatement);
  }

  private buildANDQuery(segment: UserSegmentDTO): string {
    return Object.entries(segment).reduce((acc, [key, value]) => {
      const camelCaseKey = this.toCamelCase(key);
      if (StaticDateQueryDTO.isStaticQueryDTO(value)) {
        return `${acc} AND ${this.buildStaticDateQueryCondition(
          camelCaseKey,
          value,
        )}`;
      }
      if (DynamicDateQueryDTO.isDynamicQueryDTO(value)) {
        return `${acc} AND ${this.buildDynamicDateQueryCondition(
          camelCaseKey,
          value,
        )}`;
      }
      if (isArray(value) && key === 'tags') {
        return `${acc} AND ${this.buildArrayQuery(
          `${this.TAG_ALIAS}.name`,
          value,
        )}`;
      }
      return `${acc} AND ${this.buildSimpleCondition(camelCaseKey, value)}`;
    }, '1 = 1');
  }

  private buildSimpleCondition(
    property: string,
    value: string | number | boolean,
  ): string {
    if (isNumber(value) || isBoolean(value)) {
      return `${property} = ${value}`;
    }
    return `${property} = '${value}'`;
  }

  private buildArrayQuery(property: string, value: Array<string>) {
    const initialStatement = `${property} IN (`;
    return (
      value.reduce((acc, item) => {
        if (acc === initialStatement) {
          return `${acc}'${item}'`;
        }
        return `${acc}, '${item}'`;
      }, initialStatement) + ')'
    );
  }

  private buildStaticDateQueryCondition(
    property: string,
    staticQuery: StaticDateQueryDTO,
  ): string {
    if (staticQuery.before && staticQuery.after) {
      return `${property} BETWEEN '${staticQuery.after}' AND '${staticQuery.before}'`;
    }

    if (staticQuery.before) {
      return `${property} < '${staticQuery.before}'`;
    }

    return `${property} > '${staticQuery.after}'`;
  }

  private buildDynamicDateQueryCondition(
    property: string,
    dynamicQuery: DynamicDateQueryDTO,
  ): string {
    return `${property} >= (NOW() - (INTERVAL '${dynamicQuery.amount} ${dynamicQuery.period}'))`;
  }

  private toCamelCase(value: string) {
    return value.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);
  }
}
