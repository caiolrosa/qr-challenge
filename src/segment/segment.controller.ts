import { Controller, Body, Post, Delete, Param, Get } from '@nestjs/common';
import { Segment } from './segment.entity';
import { SegmentCreateDTO } from './dto/segment-create.dto';
import { SegmentService } from './segment.service';
import {
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiBody,
} from '@nestjs/swagger';

@Controller('segments')
export class SegmentController {
  constructor(private readonly segmentService: SegmentService) {}

  @Post()
  @ApiCreatedResponse({ description: 'Segment created successfully' })
  @ApiBadRequestResponse({
    description: 'Some properties are invalid in the payload',
  })
  @ApiBody({ type: SegmentCreateDTO })
  create(@Body() segmentCreateDTO: SegmentCreateDTO): Promise<Segment> {
    return this.segmentService.create(segmentCreateDTO);
  }

  @Get()
  @ApiOkResponse({ description: 'List of all available segments' })
  read(): Promise<Segment[]> {
    return this.segmentService.findAll();
  }

  @Delete(':id')
  @ApiOkResponse({ description: 'Deleted segment successfully' })
  delete(@Param('id') id: number): Promise<void> {
    return this.segmentService.delete(id);
  }
}
