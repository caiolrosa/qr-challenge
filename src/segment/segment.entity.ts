import { PrimaryGeneratedColumn, Column, Entity } from 'typeorm';

@Entity('segments')
export class Segment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
}
