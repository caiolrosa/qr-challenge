import { Module } from '@nestjs/common';
import { SegmentController } from './segment.controller';
import { SegmentService } from './segment.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SegmentRepository } from './segment.repository';
import { SegmentQueryBuilderService } from './segment-query-builder.service';

@Module({
  imports: [TypeOrmModule.forFeature([SegmentRepository])],
  controllers: [SegmentController],
  providers: [SegmentService, SegmentQueryBuilderService],
  exports: [SegmentService, SegmentQueryBuilderService],
})
export class SegmentModule {}
