import { Repository, EntityRepository } from 'typeorm';
import { Segment } from './segment.entity';
import { User } from '../user/user.entity';

export type CreateViewQuery = string;

@EntityRepository(Segment)
export class SegmentRepository extends Repository<Segment> {
  async createSegment(
    segmentName: string,
    createViewQuery: CreateViewQuery,
  ): Promise<Segment> {
    await this.manager.transaction(async entityManager => {
      await entityManager.save(this.create({ name: segmentName }));
      await entityManager.query(createViewQuery);
    });
    return this.findOne({ where: { name: segmentName } });
  }

  async deleteSegment(id: number) {
    const segment = await this.findOne(id);
    return this.manager.transaction(async entityManager => {
      await entityManager.delete(Segment, id);
      await entityManager.query(`DROP VIEW "${segment.name}";`);
    });
  }

  findUsersBySegment(name: string): Promise<User[]> {
    return this.query(`SELECT * FROM ${name};`);
  }
}
