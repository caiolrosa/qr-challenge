import { Injectable } from '@nestjs/common';
import { SegmentRepository } from './segment.repository';
import { SegmentCreateDTO } from './dto/segment-create.dto';
import { Segment } from './segment.entity';
import { SegmentQueryBuilderService } from './segment-query-builder.service';
import { User } from '../user/user.entity';
import { NoSegmentsError } from '../errors/no-segments.error';

@Injectable()
export class SegmentService {
  constructor(
    private readonly segmentRepository: SegmentRepository,
    private readonly segmentQueryBuildService: SegmentQueryBuilderService,
  ) {}

  async create(dto: SegmentCreateDTO): Promise<Segment> {
    const viewQuery = this.segmentQueryBuildService.buildSegmentQuery(dto);
    return this.segmentRepository.createSegment(dto.name, viewQuery);
  }

  findAll(): Promise<Segment[]> {
    return this.segmentRepository.find();
  }

  findByName(name: string): Promise<Segment> {
    return this.segmentRepository.findOne({ where: { name } });
  }

  async findUsersBySegment(name: string): Promise<User[]> {
    const segment = await this.findByName(name);
    if (!segment) {
      throw new NoSegmentsError(`Segment ${name} not found`);
    }
    return this.segmentRepository.findUsersBySegment(segment.name);
  }

  async delete(id: number): Promise<void> {
    return this.segmentRepository.deleteSegment(id);
  }
}
