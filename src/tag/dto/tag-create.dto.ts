import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class TagCreateDTO {
  @IsNotEmpty()
  @ApiProperty({ example: 'Marketing' })
  name: string;
}
