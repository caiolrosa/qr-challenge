import { Controller, Post, Get, Body } from '@nestjs/common';
import { TagService } from './tag.service';
import { TagCreateDTO } from './dto/tag-create.dto';
import { Tag } from './tag.entity';
import {
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiOkResponse,
} from '@nestjs/swagger';

@Controller('tags')
export class TagController {
  constructor(private readonly tagService: TagService) {}

  @Post()
  @ApiCreatedResponse({ description: 'User created successfully' })
  @ApiBadRequestResponse({
    description: 'Some properties are invalid in the payload',
  })
  create(@Body() tagCreateDTO: TagCreateDTO): Promise<Tag> {
    return this.tagService.create(tagCreateDTO);
  }

  @Get()
  @ApiOkResponse({ description: 'List of all available tags' })
  read(): Promise<Tag[]> {
    return this.tagService.findAll();
  }
}
