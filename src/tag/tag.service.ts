import { Injectable } from '@nestjs/common';
import { TagRepository } from './tag.repository';
import { TagCreateDTO } from './dto/tag-create.dto';
import { Tag } from './tag.entity';
import { In } from 'typeorm';

@Injectable()
export class TagService {
  constructor(private readonly tagRepository: TagRepository) {}

  create(tagCreateDTO: TagCreateDTO): Promise<Tag> {
    return this.tagRepository.save(tagCreateDTO);
  }

  findAll(): Promise<Tag[]> {
    return this.tagRepository.find();
  }

  findByNames(names: string[]): Promise<Tag[]> {
    if (!names || names.length === 0) {
      return Promise.resolve([]);
    }
    return this.tagRepository.find({
      where: {
        name: In(names),
      },
    });
  }
}
