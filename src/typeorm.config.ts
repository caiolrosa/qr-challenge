import { Injectable } from '@nestjs/common';
import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  constructor(private readonly configService: ConfigService) {}
  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      name: 'challenge',
      type: 'postgres',
      host: this.configService.get('DB_HOST', 'localhost'),
      port: this.configService.get<number>('DB_PORT', 5432),
      username: this.configService.get('DB_USER', 'root'),
      password: this.configService.get('DB_PASSWORD', 'root'),
      database: this.configService.get('DB_NAME', 'challenge'),
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: false,
    };
  }
}
