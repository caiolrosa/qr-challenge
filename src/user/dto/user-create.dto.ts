import { Sex } from '../user.entity';
import {
  IsNotEmpty,
  IsEmail,
  IsDateString,
  IsBoolean,
  IsEnum,
  IsArray,
  IsOptional,
} from 'class-validator';
import { IsShortDateString } from '../../annotations/data-validation.annotation';
import { ApiProperty } from '@nestjs/swagger';

export class UserCreateDTO {
  @IsNotEmpty()
  @ApiProperty({ example: 'John' })
  firstName: string;

  @IsNotEmpty()
  @ApiProperty({ example: 'Doe' })
  lastName: string;

  @IsEmail()
  @ApiProperty({ example: 'john.doe@email.com' })
  email: string;

  @IsShortDateString()
  @ApiProperty({ example: '2020-10-05' })
  birthDate: string;

  @IsShortDateString()
  @ApiProperty({ example: '2020-10-05' })
  admissionDate: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({ example: true })
  isActive?: boolean;

  @IsEnum(Sex)
  @ApiProperty({ enum: Sex, example: 'male' })
  sex: Sex;

  @IsOptional()
  @IsDateString()
  @ApiProperty({ example: '2020-12-03T14:30:00.000Z' })
  lastSignInAt?: string;

  @IsArray()
  @ApiProperty({ type: [String] })
  tags: string[];
}
