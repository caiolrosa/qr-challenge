import { Sex } from '../user.entity';
import {
  IsOptional,
  IsEmail,
  IsDateString,
  IsBoolean,
  IsEnum,
  IsArray,
} from 'class-validator';
import { IsShortDateString } from '../../annotations/data-validation.annotation';
import { ApiProperty } from '@nestjs/swagger';

export class UserUpdateDTO {
  @IsOptional()
  @ApiProperty({ example: 'John' })
  firstName?: string;

  @IsOptional()
  @ApiProperty({ example: 'Doe' })
  lastName?: string;

  @IsOptional()
  @IsEmail()
  @ApiProperty({ example: 'john.doe@email.com' })
  email?: string;

  @IsOptional()
  @IsShortDateString()
  @ApiProperty({ example: '2020-10-05' })
  birthDate?: string;

  @IsOptional()
  @IsShortDateString()
  @ApiProperty({ example: '2020-10-05' })
  admissionDate?: string;

  @IsOptional()
  @IsBoolean()
  @ApiProperty({ example: true })
  isActive?: boolean;

  @IsOptional()
  @IsEnum(Sex)
  @ApiProperty({ enum: Sex, example: 'female' })
  sex?: Sex;

  @IsOptional()
  @IsDateString()
  @ApiProperty({ example: '2020-12-03T14:30:00.000Z' })
  lastSignInAt?: string;

  @IsOptional()
  @IsArray()
  @ApiProperty({ type: [String] })
  tags?: string[];
}
