import {
  Controller,
  Post,
  Get,
  Delete,
  Patch,
  Param,
  Body,
} from '@nestjs/common';
import { UserService } from './user.service';
import { UserCreateDTO } from './dto/user-create.dto';
import { UserUpdateDTO } from './dto/user-update.dto';
import { User } from './user.entity';
import {
  ApiOkResponse,
  ApiCreatedResponse,
  ApiBadRequestResponse,
} from '@nestjs/swagger';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @ApiCreatedResponse({ description: 'User created successfully' })
  @ApiBadRequestResponse({
    description: 'Some properties are invalid in the payload',
  })
  create(@Body() userCreateDTO: UserCreateDTO): Promise<User> {
    return this.userService.create(userCreateDTO);
  }

  @Get()
  @ApiOkResponse({ description: 'List of all saved users' })
  read(): Promise<User[]> {
    return this.userService.findAll();
  }

  @Get(':id')
  @ApiOkResponse({ description: 'User matching the provided id' })
  get(@Param('id') id: number): Promise<User> {
    return this.userService.findById(id);
  }

  @Get('segment/:name')
  @ApiOkResponse({
    description: 'List of all users matching the segment search criteria',
  })
  getBySegment(@Param('name') name: string): Promise<User[]> {
    return this.userService.findBySegmentName(name);
  }

  @Patch(':id')
  @ApiOkResponse({ description: 'User updated successfully' })
  @ApiBadRequestResponse({
    description: 'Some properties are invalid in the payload',
  })
  update(
    @Param('id') id: number,
    @Body() userUpdateDTO: UserUpdateDTO,
  ): Promise<User> {
    return this.userService.update(id, userUpdateDTO);
  }

  @Delete(':id')
  @ApiOkResponse({ description: 'User deleted successfully' })
  delete(@Param('id') id: number): Promise<void> {
    return this.userService.delete(id);
  }
}
