import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import { Tag } from '../tag/tag.entity';

export enum Sex {
  Male = 'male',
  Female = 'female',
}

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'first_name', nullable: false })
  firstName: string;

  @Column({ name: 'last_name', nullable: false })
  lastName: string;

  @Column({ nullable: false, unique: true })
  email: string;

  @Column({ name: 'birth_date', type: 'date' })
  birthDate: Date;

  @Column({ name: 'admission_date', type: 'date' })
  admissionDate: Date;

  @Column({ name: 'is_active', default: true })
  isActive: boolean;

  @Column({ type: 'enum', enum: Sex })
  sex: Sex;

  @Column({
    name: 'last_sign_in_at',
    type: 'timestamp with time zone',
    default: new Date(),
  })
  lastSignInAt: Date;

  @JoinTable({
    name: 'user_tag',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'tag_id', referencedColumnName: 'id' },
  })
  @ManyToMany(() => Tag, { onDelete: 'CASCADE' })
  tags: Tag[];
}
