import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { TagModule } from '../tag/tag.module';
import { SegmentService } from '../segment/segment.service';
import { SegmentModule } from '../segment/segment.module';
import { SegmentRepository } from '../segment/segment.repository';

@Module({
  imports: [
    TagModule,
    SegmentModule,
    TypeOrmModule.forFeature([UserRepository, SegmentRepository]),
  ],
  controllers: [UserController],
  providers: [UserService, SegmentService],
})
export class UserModule {}
