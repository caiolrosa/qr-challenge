import { Injectable } from '@nestjs/common';
import { UserRepository } from './user.repository';
import { UserCreateDTO } from './dto/user-create.dto';
import { User } from './user.entity';
import { UserUpdateDTO } from './dto/user-update.dto';
import { TagService } from '../tag/tag.service';
import { SegmentService } from '../segment/segment.service';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly tagService: TagService,
    private readonly segmentService: SegmentService,
  ) {}

  async create(dto: UserCreateDTO): Promise<User> {
    const tags = await this.tagService.findByNames(dto.tags);
    const user = this.userRepository.create({
      ...dto,
      tags: tags || [],
    });
    return this.userRepository.save(user);
  }
  findAll(): Promise<User[]> {
    return this.userRepository.find({ relations: ['tags'] });
  }
  findById(id: number): Promise<User> {
    return this.userRepository.findOne(id, { relations: ['tags'] });
  }

  findBySegmentName(name: string) {
    return this.segmentService.findUsersBySegment(name);
  }

  async update(id: number, dto: UserUpdateDTO): Promise<User> {
    const user = await this.userRepository.findOne(id);
    const tags = await this.tagService.findByNames(dto.tags);
    const mergedUser = this.userRepository.merge(user, {
      ...dto,
      tags,
    });
    await this.userRepository.save(mergedUser);
    return mergedUser;
  }
  async delete(id: number): Promise<void> {
    await this.userRepository.update(id, { isActive: false });
    return;
  }
}
