import { PsqlContainerData, IntegrationTestSetup } from '../test.setup';
import { INestApplication, HttpStatus } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { SegmentRepository } from '../../src/segment/segment.repository';
import { Segment } from '../../src/segment/segment.entity';
import { SegmentCreateDTO } from '../../src/segment/dto/segment-create.dto';
import { SegmentModule } from '../../src/segment/segment.module';
import { User } from '../../src/user/user.entity';
import { Tag } from '../../src/tag/tag.entity';
import { UserRepository } from '../../src/user/user.repository';
import { TagRepository } from '../../src/tag/tag.repository';

describe('SegmentController (e2e)', () => {
  let testApp: INestApplication;
  let psqlContainerData: PsqlContainerData;
  let segmentRepository: SegmentRepository;

  beforeAll(async () => {
    psqlContainerData = await IntegrationTestSetup.startTestContainer();
    testApp = await IntegrationTestSetup.createTestingApplication({
      imports: [
        SegmentModule,
        ConfigModule.forRoot({ isGlobal: true }),
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: psqlContainerData.dbHost,
          port: psqlContainerData.dbPort,
          username: psqlContainerData.username,
          password: psqlContainerData.password,
          database: psqlContainerData.dbName,
          entities: [User, Tag, Segment],
          synchronize: true,
        }),
        TypeOrmModule.forFeature([
          UserRepository,
          TagRepository,
          SegmentRepository,
        ]),
      ],
    });
    await testApp.init();

    segmentRepository = testApp.get(SegmentRepository);
  });

  beforeEach(async () => {
    await segmentRepository.delete({});
  });

  afterAll(async () => {
    await testApp.close();
    await psqlContainerData.container.stop();
  });

  describe('/segments (POST) contract', () => {
    it('should accept valid payload', async () => {
      const payload: SegmentCreateDTO = {
        name: 'active-users',
        segments: [
          {
            isActive: true,
          },
        ],
      };
      return request(testApp.getHttpServer())
        .post('/segments')
        .send(payload)
        .expect(HttpStatus.CREATED);
    });

    it('should reject invalid name', async () => {
      const payload: SegmentCreateDTO = {
        name: '',
        segments: [{ isActive: true }],
      };
      return request(testApp.getHttpServer())
        .post('/segments')
        .send(payload)
        .expect(HttpStatus.BAD_REQUEST);
    });

    it('should reject if any property of segments is invalid', () => {
      const payload: SegmentCreateDTO = {
        name: 'valid-name',
        segments: [
          {
            birthDate: {
              after: '2020-20-20',
            },
          },
        ],
      };
      return request(testApp.getHttpServer())
        .post('/segments')
        .send(payload)
        .expect(HttpStatus.BAD_REQUEST);
    });
  });
});
