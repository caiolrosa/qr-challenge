import { PsqlContainerData, IntegrationTestSetup } from '../test.setup';
import { INestApplication, HttpStatus } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as request from 'supertest';
import { SegmentRepository } from '../../src/segment/segment.repository';
import { Segment } from '../../src/segment/segment.entity';
import { SegmentCreateDTO } from '../../src/segment/dto/segment-create.dto';
import { SegmentModule } from '../../src/segment/segment.module';
import { User } from '../../src/user/user.entity';
import { Tag } from '../../src/tag/tag.entity';
import { UserRepository } from '../../src/user/user.repository';
import { TagRepository } from '../../src/tag/tag.repository';

describe('SegmentController (e2e)', () => {
  let testApp: INestApplication;
  let psqlContainerData: PsqlContainerData;
  let segmentRepository: SegmentRepository;

  beforeAll(async () => {
    psqlContainerData = await IntegrationTestSetup.startTestContainer();
    testApp = await IntegrationTestSetup.createTestingApplication({
      imports: [
        SegmentModule,
        ConfigModule.forRoot({ isGlobal: true }),
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: psqlContainerData.dbHost,
          port: psqlContainerData.dbPort,
          username: psqlContainerData.username,
          password: psqlContainerData.password,
          database: psqlContainerData.dbName,
          entities: [User, Tag, Segment],
          synchronize: true,
        }),
        TypeOrmModule.forFeature([
          UserRepository,
          TagRepository,
          SegmentRepository,
        ]),
      ],
    });
    await testApp.init();

    segmentRepository = testApp.get(SegmentRepository);
  });

  beforeEach(async () => {
    await segmentRepository.delete({});
  });

  afterAll(async () => {
    await testApp.close();
    await psqlContainerData.container.stop();
  });

  describe('/segments (POST)', () => {
    it('should create segment', async () => {
      const payload: SegmentCreateDTO = {
        name: 'active-users',
        segments: [
          {
            isActive: true,
          },
        ],
      };
      const { body } = await request(testApp.getHttpServer())
        .post('/segments')
        .send(payload)
        .expect(HttpStatus.CREATED);
      expect(body.name).toEqual(payload.name);
    });
  });

  describe('/segments (GET)', () => {
    it('should return all segments', async () => {
      const savedSegments = await segmentRepository.save([
        { name: 'active-users' },
        { name: 'recently-admitted' },
        { name: 'female-users' },
      ]);
      const { body } = await request(testApp.getHttpServer())
        .get('/segments')
        .expect(HttpStatus.OK);
      expect(body).toMatchObject(savedSegments);
    });
  });

  describe('/segments/:id (DELETE)', () => {
    it('should delete a segment', async () => {
      const savedSegment = await segmentRepository.save({
        name: 'active-users',
      });
      await request(testApp.getHttpServer())
        .delete(`/segments/${savedSegment.id}`)
        .expect(HttpStatus.OK);
      expect(segmentRepository.findOne(savedSegment.id)).rejects.toBeDefined();
      expect(
        segmentRepository.query(`
        SELECT table_name FROM INFORMATION_SCHEMA.views 
        WHERE table_name = ${savedSegment.name};
      `),
      ).rejects.toBeDefined();
    });
  });
});
