import { PsqlContainerData, IntegrationTestSetup } from '../test.setup';
import { INestApplication, HttpStatus } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tag } from '../../src/tag/tag.entity';
import { TagRepository } from '../../src/tag/tag.repository';
import { TagModule } from '../../src/tag/tag.module';
import * as request from 'supertest';
import { TagCreateDTO } from '../../src/tag/dto/tag-create.dto';

describe('TagController (e2e)', () => {
  let testApp: INestApplication;
  let psqlContainerData: PsqlContainerData;
  let tagRepository: TagRepository;

  beforeAll(async () => {
    psqlContainerData = await IntegrationTestSetup.startTestContainer();
    testApp = await IntegrationTestSetup.createTestingApplication({
      imports: [
        TagModule,
        ConfigModule.forRoot({ isGlobal: true }),
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: psqlContainerData.dbHost,
          port: psqlContainerData.dbPort,
          username: psqlContainerData.username,
          password: psqlContainerData.password,
          database: psqlContainerData.dbName,
          entities: [Tag],
          synchronize: true,
        }),
        TypeOrmModule.forFeature([TagRepository]),
      ],
    });
    await testApp.init();

    tagRepository = testApp.get(TagRepository);
  });

  beforeEach(async () => {
    await tagRepository.delete({});
  });

  afterAll(async () => {
    await testApp.close();
    await psqlContainerData.container.stop();
  });

  describe('/tags (POST)', () => {
    it('should create tag', async () => {
      const payload: TagCreateDTO = { name: 'Design' };
      const { body } = await request(testApp.getHttpServer())
        .post('/tags')
        .send(payload)
        .expect(HttpStatus.CREATED);
      expect(body.name).toEqual(payload.name);
    });
  });

  describe('/tags (GET)', () => {
    it('should return all tags', async () => {
      const savedTags = await tagRepository.save([
        { name: 'Design' },
        { name: 'HR' },
        { name: 'Engineering' },
      ]);
      const { body } = await request(testApp.getHttpServer())
        .get('/tags')
        .expect(HttpStatus.OK);
      expect(body).toMatchObject(savedTags);
    });
  });
});
