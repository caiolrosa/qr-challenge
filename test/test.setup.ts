import { GenericContainer, StartedTestContainer } from 'testcontainers';
import { ModuleMetadata, INestApplication } from '@nestjs/common/interfaces';
import { Test } from '@nestjs/testing';
import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

const POSTGRES_DB_PORT = 5432;
export interface PsqlContainerData {
  username: string;
  password: string;
  dbName: string;
  dbHost: string;
  dbPort: number;
  container: StartedTestContainer;
}

export class IntegrationTestSetup {
  static async startTestContainer(): Promise<PsqlContainerData> {
    const dbParam = 'test';
    const container = await new GenericContainer('postgres')
      .withName(`psql-test-container-${new Date().getTime()}`)
      .withEnv('POSTGRES_USER', dbParam)
      .withEnv('POSTGRES_PASSWORD', dbParam)
      .withEnv('POSTGRES_DB', dbParam)
      .withExposedPorts(POSTGRES_DB_PORT)
      .start();

    return {
      username: dbParam,
      password: dbParam,
      dbName: dbParam,
      dbHost: 'localhost',
      dbPort: container.getMappedPort(POSTGRES_DB_PORT),
      container: container,
    };
  }

  static async createTestingApplication(
    metadata: ModuleMetadata,
  ): Promise<INestApplication> {
    const testingModule = await Test.createTestingModule(metadata).compile();
    const testApp = await testingModule.createNestApplication();
    testApp.useGlobalInterceptors(
      new ClassSerializerInterceptor(testApp.get(Reflector)),
    );
    testApp.useGlobalPipes(new ValidationPipe({ transform: true }));
    return testApp;
  }
}
