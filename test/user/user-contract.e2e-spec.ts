import { PsqlContainerData, IntegrationTestSetup } from '../test.setup';
import { INestApplication, HttpStatus } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User, Sex } from '../../src/user/user.entity';
import { Tag } from '../../src/tag/tag.entity';
import { UserRepository } from '../../src/user/user.repository';
import { UserCreateDTO } from '../../src/user/dto/user-create.dto';
import { TagRepository } from '../../src/tag/tag.repository';
import * as request from 'supertest';
import { UserModule } from '../../src/user/user.module';
import { SegmentRepository } from '../../src/segment/segment.repository';
import { Segment } from '../../src/segment/segment.entity';

describe('UserController Contract (e2e)', () => {
  let testApp: INestApplication;
  let psqlContainerData: PsqlContainerData;
  let userRepository: UserRepository;
  let tagRepository: TagRepository;

  beforeAll(async () => {
    psqlContainerData = await IntegrationTestSetup.startTestContainer();
    testApp = await IntegrationTestSetup.createTestingApplication({
      imports: [
        UserModule,
        ConfigModule.forRoot({ isGlobal: true }),
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: psqlContainerData.dbHost,
          port: psqlContainerData.dbPort,
          username: psqlContainerData.username,
          password: psqlContainerData.password,
          database: psqlContainerData.dbName,
          entities: [User, Tag, Segment],
          synchronize: true,
        }),
        TypeOrmModule.forFeature([
          UserRepository,
          TagRepository,
          SegmentRepository,
        ]),
      ],
    });
    await testApp.init();

    userRepository = testApp.get(UserRepository);
    tagRepository = testApp.get(TagRepository);
    await createDefaultTags();
  });

  beforeEach(async () => {
    await userRepository.delete({});
  });

  afterAll(async () => {
    await testApp.close();
    await psqlContainerData.container.stop();
  });

  async function createDefaultTags() {
    await tagRepository.save([
      { name: 'Engineering' },
      { name: 'Design' },
      { name: 'HR' },
    ]);
  }

  describe('/users (POST) contract', () => {
    it('should accept valid payload', async () => {
      const payload: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.CREATED);
    });

    it('should accept isActive not provided', async () => {
      const payload: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.CREATED);
    });

    it('should accept lastSignInAt not provided', async () => {
      const payload: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.CREATED);
    });

    it('should accept empty tags', async () => {
      const payload: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: [],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.CREATED);
    });

    it('should reject invalid firstName', async () => {
      const payload: UserCreateDTO = {
        firstName: '',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.BAD_REQUEST);
    });

    it('should reject invalid lastName', async () => {
      const payload: UserCreateDTO = {
        firstName: 'Some',
        lastName: '',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.BAD_REQUEST);
    });

    it('should reject invalid email', async () => {
      const payload: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'not-an-email',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.BAD_REQUEST);
    });

    it('should reject invalid admissionDate', async () => {
      const payload: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-30-11',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.BAD_REQUEST);
    });

    it('should reject invalid birthDate', async () => {
      const payload: UserCreateDTO = {
        firstName: '',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-30-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.BAD_REQUEST);
    });

    it('should reject invalid isActive', async () => {
      const payload: UserCreateDTO = {
        firstName: '',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send({ ...payload, isActive: 'true' })
        .expect(HttpStatus.BAD_REQUEST);
    });

    it('should reject invalid sex', async () => {
      const payload: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send({ ...payload, sex: 'invalid-sex' })
        .expect(HttpStatus.BAD_REQUEST);
    });

    it('should reject invalid tags', async () => {
      const payload: UserCreateDTO = {
        firstName: '',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send({ ...payload, tags: 'invalid, tags' })
        .expect(HttpStatus.BAD_REQUEST);
    });

    it('should reject invalid lastSignInAt', async () => {
      const payload: UserCreateDTO = {
        firstName: '',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-30-18T01:40:00.000Z',
      };
      return request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.BAD_REQUEST);
    });
  });
});
