import { PsqlContainerData, IntegrationTestSetup } from '../test.setup';
import { INestApplication, HttpStatus } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User, Sex } from '../../src/user/user.entity';
import { Tag } from '../../src/tag/tag.entity';
import { UserRepository } from '../../src/user/user.repository';
import { TagRepository } from '../../src/tag/tag.repository';
import * as request from 'supertest';
import { UserModule } from '../../src/user/user.module';
import { SegmentRepository } from '../../src/segment/segment.repository';
import { Segment } from '../../src/segment/segment.entity';
import { UserService } from '../../src/user/user.service';
import { SegmentModule } from '../../src/segment/segment.module';
import { SegmentService } from '../../src/segment/segment.service';
import { DatePeriod } from '../../src/segment/dto/dynamic-date-query.dto';

describe('UserController (e2e)', () => {
  let testApp: INestApplication;
  let psqlContainerData: PsqlContainerData;
  let userService: UserService;
  let userRepository: UserRepository;
  let tagRepository: TagRepository;
  let segmentService: SegmentService;

  beforeAll(async () => {
    psqlContainerData = await IntegrationTestSetup.startTestContainer();
    testApp = await IntegrationTestSetup.createTestingApplication({
      imports: [
        UserModule,
        SegmentModule,
        ConfigModule.forRoot({ isGlobal: true }),
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: psqlContainerData.dbHost,
          port: psqlContainerData.dbPort,
          username: psqlContainerData.username,
          password: psqlContainerData.password,
          database: psqlContainerData.dbName,
          entities: [User, Tag, Segment],
          synchronize: true,
        }),
        TypeOrmModule.forFeature([
          UserRepository,
          TagRepository,
          SegmentRepository,
        ]),
      ],
    });
    await testApp.init();

    userService = testApp.get(UserService);
    userRepository = testApp.get(UserRepository);
    tagRepository = testApp.get(TagRepository);
    segmentService = testApp.get(SegmentService);
    await createDefaultTags();
  });

  beforeEach(async () => {
    await userRepository.delete({});
  });

  afterAll(async () => {
    await testApp.close();
    await psqlContainerData.container.stop();
  });

  async function createDefaultTags() {
    await tagRepository.save([
      { name: 'Marketing' },
      { name: 'Sales' },
      { name: 'Engineering' },
      { name: 'Design' },
      { name: 'RH' },
    ]);
  }

  describe('/users/segment/:name (GET)', () => {
    it('should create segment and find users for active users admitted between 2016 and 2017', async () => {
      const user1 = await userService.create({
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2016-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: [],
      });
      const user2 = await userService.create({
        firstName: 'Any',
        lastName: 'One',
        email: 'any.one@email.com',
        admissionDate: '2017-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: [],
      });
      const segment = await segmentService.create({
        name: 'admitted_2016_2017',
        segments: [
          {
            isActive: true,
            admissionDate: { before: '2017-01-01', after: '2016-01-01' },
          },
        ],
      });
      const { body } = await request(testApp.getHttpServer())
        .get(`/users/segment/${segment.name}`)
        .expect(HttpStatus.OK);
      expect(body).toHaveLength(2);
      expect(body[0].email).toEqual(user1.email);
      expect(body[1].email).toEqual(user2.email);
    });
  });
  it('should create segment and find male users with Marketing tag or female users with Sales tag', async () => {
    const user1 = await userService.create({
      firstName: 'Some',
      lastName: 'One',
      email: 'some.one@email.com',
      admissionDate: '2016-01-01',
      birthDate: '1990-04-05',
      isActive: true,
      sex: Sex.Male,
      tags: ['Marketing'],
    });
    const user2 = await userService.create({
      firstName: 'Any',
      lastName: 'One',
      email: 'any.one@email.com',
      admissionDate: '2017-01-01',
      birthDate: '1990-04-05',
      isActive: true,
      sex: Sex.Female,
      tags: ['Sales'],
    });
    const segment = await segmentService.create({
      name: 'male_female_tagged',
      segments: [
        {
          sex: Sex.Male,
          tags: ['Marketing'],
        },
        {
          sex: Sex.Female,
          tags: ['Sales'],
        },
      ],
    });
    const { body } = await request(testApp.getHttpServer())
      .get(`/users/segment/${segment.name}`)
      .expect(HttpStatus.OK);
    expect(body).toHaveLength(2);
    expect(body[0].email).toEqual(user1.email);
    expect(body[1].email).toEqual(user2.email);
  });
  it('should create segment and find users that last signed in the last 3 days', async () => {
    const user1 = await userService.create({
      firstName: 'Some',
      lastName: 'One',
      email: 'some.one@email.com',
      admissionDate: '2016-01-01',
      birthDate: '1990-04-05',
      isActive: true,
      sex: Sex.Male,
      tags: [],
    });
    const segment = await segmentService.create({
      name: 'recent_sign_in',
      segments: [
        {
          lastSignInAt: {
            amount: 3,
            period: DatePeriod.Days,
          },
        },
      ],
    });
    const { body } = await request(testApp.getHttpServer())
      .get(`/users/segment/${segment.name}`)
      .expect(HttpStatus.OK);
    expect(body).toHaveLength(1);
    expect(body[0].email).toEqual(user1.email);
  });
  it('should create segment and find users tagged as: RH, Engineering, Design', async () => {
    const user1 = await userService.create({
      firstName: 'Some',
      lastName: 'One',
      email: 'some.one@email.com',
      admissionDate: '2016-01-01',
      birthDate: '1990-04-05',
      isActive: true,
      sex: Sex.Male,
      tags: ['RH'],
    });
    const user2 = await userService.create({
      firstName: 'Any',
      lastName: 'One',
      email: 'any.one@email.com',
      admissionDate: '2016-01-01',
      birthDate: '1990-04-05',
      isActive: true,
      sex: Sex.Male,
      tags: ['Engineering'],
    });
    const user3 = await userService.create({
      firstName: 'Some',
      lastName: 'Body',
      email: 'some.body@email.com',
      admissionDate: '2016-01-01',
      birthDate: '1990-04-05',
      isActive: true,
      sex: Sex.Male,
      tags: ['Design'],
    });
    const segment = await segmentService.create({
      name: 'recent_sign_in',
      segments: [
        {
          tags: ['RH', 'Engineering', 'Design'],
        },
      ],
    });
    const { body } = await request(testApp.getHttpServer())
      .get(`/users/segment/${segment.name}`)
      .expect(HttpStatus.OK);
    expect(body).toHaveLength(3);
    expect(body[0].email).toEqual(user1.email);
    expect(body[1].email).toEqual(user2.email);
    expect(body[2].email).toEqual(user3.email);
  });
});
