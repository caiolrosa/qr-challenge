import { PsqlContainerData, IntegrationTestSetup } from '../test.setup';
import { INestApplication, HttpStatus } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User, Sex } from '../../src/user/user.entity';
import { Tag } from '../../src/tag/tag.entity';
import { UserRepository } from '../../src/user/user.repository';
import { UserCreateDTO } from '../../src/user/dto/user-create.dto';
import { TagRepository } from '../../src/tag/tag.repository';
import * as request from 'supertest';
import { UserModule } from '../../src/user/user.module';
import { UserUpdateDTO } from '../../src/user/dto/user-update.dto';
import { SegmentRepository } from '../../src/segment/segment.repository';
import { Segment } from '../../src/segment/segment.entity';

describe('UserController (e2e)', () => {
  let testApp: INestApplication;
  let psqlContainerData: PsqlContainerData;
  let userRepository: UserRepository;
  let tagRepository: TagRepository;

  beforeAll(async () => {
    psqlContainerData = await IntegrationTestSetup.startTestContainer();
    testApp = await IntegrationTestSetup.createTestingApplication({
      imports: [
        UserModule,
        ConfigModule.forRoot({ isGlobal: true }),
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: psqlContainerData.dbHost,
          port: psqlContainerData.dbPort,
          username: psqlContainerData.username,
          password: psqlContainerData.password,
          database: psqlContainerData.dbName,
          entities: [User, Tag, Segment],
          synchronize: true,
        }),
        TypeOrmModule.forFeature([
          UserRepository,
          TagRepository,
          SegmentRepository,
        ]),
      ],
    });
    await testApp.init();

    userRepository = testApp.get(UserRepository);
    tagRepository = testApp.get(TagRepository);
    await createDefaultTags();
  });

  beforeEach(async () => {
    await userRepository.delete({});
  });

  afterAll(async () => {
    await testApp.close();
    await psqlContainerData.container.stop();
  });

  async function createDefaultTags() {
    await tagRepository.save([
      { name: 'Engineering' },
      { name: 'Design' },
      { name: 'HR' },
    ]);
  }

  describe('/users (POST)', () => {
    it('should create user without providing lastSignInAt', async () => {
      const payload: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: [],
      };
      const { body } = await request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.CREATED);
      expect(body).toMatchObject(payload);
      expect(body.lastSignInAt).toBeDefined();
    });

    it('should create user providing lastSignInAt and tags', async () => {
      const payload: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-01-01',
        birthDate: '1990-04-05',
        isActive: true,
        sex: Sex.Male,
        tags: ['Engineering'],
        lastSignInAt: '2020-08-18T01:40:00.000Z',
      };
      const { body } = await request(testApp.getHttpServer())
        .post('/users')
        .send(payload)
        .expect(HttpStatus.CREATED);
      expect(body.lastSignInAt).toEqual(payload.lastSignInAt);
      expect(body.tags).toHaveLength(1);
      expect(body.tags[0].name).toEqual(payload.tags[0]);
    });
  });

  describe('/users (GET)', () => {
    it('should return all users', async () => {
      const userCreateDTO: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-08-08',
        birthDate: '1988-04-03',
        isActive: true,
        sex: Sex.Male,
        tags: [],
      };
      const tags = await tagRepository.find();
      const users = [
        {
          ...userCreateDTO,
          tags: [tags[0]],
        },
        {
          ...userCreateDTO,
          firstName: 'Another',
          lastName: 'User',
          email: 'another.user@email.com',
          sex: Sex.Female,
          tags: [tags[1]],
        },
      ];
      await userRepository.save(users);
      const { body } = await request(testApp.getHttpServer())
        .get('/users')
        .expect(HttpStatus.OK);

      expect(body).toHaveLength(users.length);
      expect(body[0].email).toEqual(users[0].email);
      expect(body[1].email).toEqual(users[1].email);
    });
  });

  describe('/users/:id (GET)', () => {
    it('should return specific user', async () => {
      const userCreateDTO: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-08-08',
        birthDate: '1988-04-03',
        isActive: true,
        sex: Sex.Male,
        tags: [],
      };
      const user = await userRepository.save({ ...userCreateDTO, tags: [] });
      const { body } = await request(testApp.getHttpServer())
        .get(`/users/${user.id}`)
        .expect(HttpStatus.OK);

      expect(body.id).toEqual(user.id);
    });
  });

  describe('/users/:id (PATCH)', () => {
    it('should update a single property', async () => {
      const userCreateDTO: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-08-08',
        birthDate: '1988-04-03',
        isActive: true,
        sex: Sex.Male,
        tags: [],
      };
      const user = await userRepository.save({ ...userCreateDTO, tags: [] });
      const payload: UserUpdateDTO = {
        firstName: 'Updated',
      };
      const { body } = await request(testApp.getHttpServer())
        .patch(`/users/${user.id}`)
        .send(payload)
        .expect(HttpStatus.OK);
      expect(body.id).toEqual(user.id);
      expect(body.firstName).toEqual(payload.firstName);
    });

    it('should update multiple properties', async () => {
      const userCreateDTO: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-08-08',
        birthDate: '1988-04-03',
        isActive: true,
        sex: Sex.Male,
        tags: [],
      };
      const user = await userRepository.save({ ...userCreateDTO, tags: [] });
      const payload: UserUpdateDTO = {
        firstName: 'Updated',
        lastName: 'Entry',
        email: 'updated.entry@email.com',
      };
      const { body } = await request(testApp.getHttpServer())
        .patch(`/users/${user.id}`)
        .send(payload)
        .expect(HttpStatus.OK);
      expect(body.id).toEqual(user.id);
      expect(body.firstName).toEqual(payload.firstName);
      expect(body.lastName).toEqual(payload.lastName);
      expect(body.email).toEqual(payload.email);
    });
  });

  describe('/users/:id (DELETE)', () => {
    it('should soft delete', async () => {
      const userCreateDTO: UserCreateDTO = {
        firstName: 'Some',
        lastName: 'One',
        email: 'some.one@email.com',
        admissionDate: '2020-08-08',
        birthDate: '1988-04-03',
        isActive: true,
        sex: Sex.Male,
        tags: [],
      };
      const user = await userRepository.save({ ...userCreateDTO, tags: [] });
      await request(testApp.getHttpServer())
        .delete(`/users/${user.id}`)
        .expect(HttpStatus.OK);
      const deletedUser = await userRepository.findOne(user.id);
      expect(deletedUser.isActive).toEqual(false);
    });
  });
});
